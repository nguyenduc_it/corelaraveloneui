@extends('admin.layouts.app')

@push('css_before')
@endpush

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Admin Account <small class="font-size-base font-w400 text-muted">Admin Account</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.admin_manager.index')}}">Account</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Admin Account</h3>
                    <div class="block-options">
                        <div class="block-options-item">
                            <code>
                                <a href="{{ route('admin.admin_manager.create')}}" class="btn btn-primary">Add</a>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th class="d-none d-sm-table-cell">Roles</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <th class="text-center" scope="row">1</th>
                                <td class="font-w600 font-size-sm">
                                    <a href="javascript void(0)">{{ $user->name }}</a>
                                </td>
                                <td class="font-w600 font-size-sm">
                                    {{ $user->email }}
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <span class="badge badge-success">{{ join(', ', $user->roles->pluck('name')->toArray()) }}</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.admin_manager.edit', $user->id) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit User">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @if($users->isEmpty())
                                <tr>
                                    <td colspan="4" class="text-center">{{__('Không có dữ liệu')}}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')

@endpush