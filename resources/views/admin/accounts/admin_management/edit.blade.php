@extends('admin.layouts.app')

@push('css_before')
<link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css')}}">
@endpush

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Edit Account Admin<small class="font-size-base font-w400 text-muted">Edit Account</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.home')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.admin_manager.index')}}">Admin Account</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.admin_manager.edit', $user->id) }}">Edit Account</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Edit Account</h3>
                </div>
                <div class="block-content block-content-full">
                    {{ Form::model($user, ['route' => ['admin.admin_manager.update', $user->id], 'method' => 'PUT']) }}
                        <div class="row push">
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }} <span class="text-danger">*</span>
                                    {{ Form::text('name', null, array('class' => 'form-control', 
                                        'id' => 'name', 
                                        'placeholder' => 'Name')) }}
                                    @error('name')
                                        <div class="alert alert-danger alert-dismissable" role="alert">
                                            <p class="mb-0">{{ $message }}</p>
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    {{ Form::label('email', 'Email') }} <span class="text-danger">*</span>
                                    {{ Form::text('email', null, array('class' => 'form-control', 
                                        'id' => 'email', 
                                        'placeholder' => 'Email')) }}
                                    @error('email')
                                        <div class="alert alert-danger alert-dismissable" role="alert">
                                            <p class="mb-0">{{ $message }}</p>
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    {{ Form::label('roles', 'Roles') }}
                                    {{ Form::select('roles[]', $roles->pluck('name', 'id'), $user->roles->pluck('id')->toArray(), ['class' => 'form-control', 'id' => 'roles', 'multiple' => true]) }}
                                </div>

                                <br>
                                <hr>

                                <div class="form-group">
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', ['class' => 'form-control']) }}
                                    @error('password')
                                        <div class="alert alert-danger alert-dismissable" role="alert">
                                            <p class="mb-0">{{ $message }}</p>
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    {{ Form::label('password', 'Confirm Password') }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                                </div>

                                
                                <hr>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Save
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-alt-secondary">
                                            <i class="fa fa-fw fa-times-circle mr-1"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('/js/plugins/select2/js/select2.full.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#roles').select2({placeholder:'', allowClear: true});
        });
    </script>
@endpush