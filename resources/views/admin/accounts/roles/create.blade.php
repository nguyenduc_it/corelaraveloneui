@extends('admin.layouts.app')

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Create Role <small class="font-size-base font-w400 text-muted">Role</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.home')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.roles.index')}}">Roles</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.roles.create') }}">Create Role</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Create Role</h3>
                </div>
                <div class="block-content block-content-full">
                    {{ Form::open(['route' => 'admin.roles.store', 'method' => 'post']) }}
                        <div class="row push">
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    {{ Form::label('role_name', 'Name') }} <span class="text-danger">*</span>
                                    {{ Form::text('name', null, array('class' => 'form-control', 
                                        'id' => 'role_name', 
                                        'placeholder' => 'Role Name')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('role_guard', 'Guard') }} <span class="text-danger">*</span>
                                    {{ Form::select('guard_name', $guards, null, array('class' => 'form-control', 'id' => 'role_guard')) }}
                                </div>
                                @if(!$permissions->isEmpty())
                                    <div class="form-group box-permission">
                                        <label>Permissions</label>
                                        <div class="form-check">
                                            {{ Form::checkbox('permissions[]',  null, null, ['id' => 'permission_all', 'class' => 'form-check-input']) }}
                                            {{ Form::label('permission_all', __('Tất cả'), ['class' => 'form-check-label']) }}
                                        </div>
                                        @foreach ($permissions as $permission)
                                            <div class="form-check guard_item {{'guard_' . $permission->guard_name}}">
                                                {{ Form::checkbox('permissions[]',  $permission->id, null, ['id' => 'permission_' . $permission->id, 'class' => 'form-check-input permission_checkbox']) }}
                                                {{ Form::label('permission_' . $permission->id, ucfirst($permission->name), ['class' => 'form-check-label']) }}
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <hr>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Save
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-alt-secondary">
                                            <i class="fa fa-fw fa-times-circle mr-1"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var displayPermission = function () {

            var guard_name = 'guard_'+$('select[name=guard_name]').val();

            $('.guard_item').hide();
            $('.box-permission .guard_item input').prop('disabled', true);
            $('.box-permission .guard_item input').prop('checked', false);

            //show in guard
            $('.'+guard_name).show();
            $('.box-permission .'+guard_name+' input').prop('disabled', false);
            $('#permission_all').prop('checked', false);
        };

        var checkAllStatus = function(){
            var permissionCount = $('.permission_checkbox').not(':disabled').length;
            var permissionSelectedCount = $('.permission_checkbox:checked').length;
            $('#permission_all').prop('checked', permissionCount === permissionSelectedCount);
        };

        $(document).ready(function () {
            displayPermission();
            checkAllStatus();

            $('select[name=guard_name]').change(displayPermission);

            $('#permission_all').change(function () {
                $(this).closest('.box-permission').find('input:checkbox').not(this).not(':disabled').prop('checked', this.checked);
            });

            $('.permission_checkbox').change(checkAllStatus);
        });
    </script>
@endpush
