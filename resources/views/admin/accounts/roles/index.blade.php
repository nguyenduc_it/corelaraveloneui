@extends('admin.layouts.app')

@push('css_before')
<link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css')}}">
@endpush

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Roles <small class="font-size-base font-w400 text-muted">Role</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.roles.index')}}">Roles</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Roles</h3>
                    <div class="block-options">
                        <div class="block-options-item">
                            <code>
                                <a href="{{ route('admin.roles.create')}}" class="btn btn-primary">Add</a>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th>Name</th>
                                <th>Guard</th>
                                <th class="d-none d-sm-table-cell">Permission</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <th class="text-center" scope="row">1</th>
                                <td class="font-w600 font-size-sm">
                                    <a href="javascript void(0)">{{ $role->name }}</a>
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <span class="badge badge-success">{{ $role->guard_name }}</span>
                                </td>
                                <td>
                                    {{  $role->permissions()->pluck('name')->implode(', ') }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-sm btn-light" data-toggle="tooltip" title="Edit Role">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </a>
                                        <button type="button" class="btn btn-sm btn-light btn-delete-role" data-role_id="{{$role->id}}" data-toggle="tooltip" title="Remove Role">
                                            <i class="fa fa-fw fa-times"></i>
                                        </button>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.roles.destroy', $role->id], 'id' => 'form-delete-' . $role->id ]) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<!-- Page JS Plugins -->
<script src="{{ asset('/js/plugins/es6-promise/es6-promise.auto.min.js')}}"></script>
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script>
    $(function(){
        // Set default properties
        let toast = Swal.mixin({
            buttonsStyling: false,
            customClass: {
                confirmButton: 'btn btn-success m-1',
                cancelButton: 'btn btn-danger m-1',
                input: 'form-control'
            }
        });

        $(document).on('click', '.btn-delete-role', function(){
            var id =  $(this).data('role_id');
            toast.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this role!',
                icon: 'warning',
                showCancelButton: true,
                customClass: {
                    confirmButton: 'btn btn-danger m-1',
                    cancelButton: 'btn btn-secondary m-1'
                },
                confirmButtonText: 'Yes, delete it!',
                html: false,
                preConfirm: e => {
                    $('#form-delete-'+id).submit();
                }
            }).then(result => {
                if (result.value) {
                    toast.fire('Deleted!', 'Role has been deleted.', 'success');
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {
                    toast.fire('Cancelled', 'Role is safe :)', 'error');
                }
            });
        });
    });
</script>
@endpush