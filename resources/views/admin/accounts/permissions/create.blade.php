@extends('admin.layouts.app')

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Create Permission <small class="font-size-base font-w400 text-muted">Permission</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.home')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.permissions.index')}}">Permission</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.permissions.create') }}">Create Permission</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Create Permission</h3>
                </div>
                <div class="block-content block-content-full">
                    {{ Form::open(['route' => 'admin.permissions.store', 'method' => 'post']) }}
                        <div class="row push">
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    {{ Form::label('permission_name', 'Name') }} <span class="text-danger">*</span>
                                    {{ Form::text('name', null, array('class' => 'form-control', 
                                        'id' => 'permission_name', 
                                        'placeholder' => 'Permission Name')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('permission_guard', 'Guard') }} <span class="text-danger">*</span>
                                    {{ Form::select('guard_name', $guards, null, array('class' => 'form-control', 'id' => 'permission_guard')) }}
                                </div>
                                @if(!$roles->isEmpty())
                                    <div class="form-group">
                                        <label>Assign Permission to Roles</label>
                                        @foreach ($roles as $role)
                                            <div class="form-check">
                                                {{ Form::checkbox('roles[]',  $role->id, null, ['id' => 'role_' . $role->id, 'class' => 'form-check-input']) }}
                                                {{ Form::label('role_' . $role->id, ucfirst($role->name), ['class' => 'form-check-label']) }}
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <hr>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Save
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-alt-secondary">
                                            <i class="fa fa-fw fa-times-circle mr-1"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    {{-- Todo update --}}
    <script type="text/javascript">
        var displayPermission = function () {
            if($('select[name=guard_name]').val() === 'admin'){
                $('.guard_user').hide();
                $('.guard_admin').show();
                $('.border-checkbox-section .guard_admin input').prop('disabled', false);
                $('.border-checkbox-section .guard_user input').prop('disabled', true);
                $('.border-checkbox-section .guard_user input').prop('checked', false);
            } else {
                $('.guard_user').show();
                $('.guard_admin').hide();
                $('.border-checkbox-section .guard_admin input').prop('disabled', true);
                $('.border-checkbox-section .guard_admin input').prop('checked', false);
                $('.border-checkbox-section .guard_user input').prop('disabled', false);
            }
            $('#permission_all').prop('checked', false);
        };

        $(document).ready(function () {
            displayPermission();
            $('select[name=guard_name]').change(displayPermission);
        });
    </script>
@endpush