@extends('admin.layouts.app')

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Edit Permission <small class="font-size-base font-w400 text-muted">Permission</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.home')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{ route('admin.permissions.index')}}">Permission</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="{{ route('admin.permissions.edit', $permission->id) }}">Edit Permission</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>


<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Create Permission</h3>
                </div>
                <div class="block-content block-content-full">
                    {{ Form::model($permission, ['route' => ['admin.permissions.update', $permission->id], 'method' => 'PUT']) }}
                        <div class="row push">
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    {{ Form::label('permission_name', 'Name') }} <span class="text-danger">*</span>
                                    {{ Form::text('name', null, array('class' => 'form-control', 
                                        'id' => 'permission_name', 
                                        'placeholder' => 'Permission Name')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('permission_guard', 'Guard') }} <span class="text-danger">*</span>
                                    {{ Form::select('guard_name', $guards, null, array('class' => 'form-control', 'id' => 'permission_guard')) }}
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Update
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-alt-secondary">
                                            <i class="fa fa-fw fa-times-circle mr-1"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection