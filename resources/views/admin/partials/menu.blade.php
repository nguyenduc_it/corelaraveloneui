<ul class="nav-main">
    <li class="nav-main-item">
        <a class="nav-main-link{{ request()->is('admin') ? ' active' : '' }}" href="{{ route('admin.home')}}">
            <i class="nav-main-link-icon si si-cursor"></i>
            <span class="nav-main-link-name">Dashboard</span>
        </a>
    </li>
    <li class="nav-main-heading">Account</li>

    <li class="nav-main-item{{ (request()->is('admin/roles') || request()->is('admin/permissions')) ? ' open' : '' }}">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
            <i class="nav-main-link-icon fa fa-user-lock"></i>
            <span class="nav-main-link-name">Roles/Permission</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('admin/roles/*') ? ' active' : '' }}" href="{{ route('admin.roles.index') }}">
                    <span class="nav-main-link-name">Role</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('admin/permissions/*') ? ' active' : '' }}" href="{{ route('admin.permissions.index') }}">
                    <span class="nav-main-link-name">Permission</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-main-item{{ request()->is('admin/admin_manager/*') ? ' open' : '' }}">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
            <i class="nav-main-link-icon fa fa-users"></i>
            <span class="nav-main-link-name">Admin Account</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('admin/admin_manager') ? ' active' : '' }}" href="{{ route('admin.admin_manager.index') }}">
                    <span class="nav-main-link-name">All Account</span>
                </a>
            </li>
        </ul>
    </li>
    
    {{-- <li class="nav-main-item{{ request()->is('pages/*') ? ' open' : '' }}">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
            <i class="nav-main-link-icon si si-bulb"></i>
            <span class="nav-main-link-name">Examples</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('pages/datatables') ? ' active' : '' }}" href="/pages/datatables">
                    <span class="nav-main-link-name">DataTables</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('pages/slick') ? ' active' : '' }}" href="/pages/slick">
                    <span class="nav-main-link-name">Slick Slider</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link{{ request()->is('pages/blank') ? ' active' : '' }}" href="/pages/blank">
                    <span class="nav-main-link-name">Blank</span>
                </a>
            </li>
        </ul>
    </li>  --}}
    {{-- <li class="nav-main-heading">More</li>
    <li class="nav-main-item open">
        <a class="nav-main-link" href="/">
            <i class="nav-main-link-icon si si-globe"></i>
            <span class="nav-main-link-name">Landing</span>
        </a>
    </li> --}}
</ul>