@extends('admin.layouts.simple')

@section('content')
<!-- Page Content -->
<div class="hero-static">
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-4">
                <!-- Sign Up Block -->
                <div class="block block-themed block-fx-shadow mb-0">
                    <div class="block-header bg-success">
                        <h3 class="block-title">{{ __('Reset Password') }}</h3>
                        <div class="block-options">
                            <a class="btn-block-option font-size-sm" href="javascript:void(0)" data-toggle="modal" data-target="#one-signup-terms">View Terms</a>
                            <a class="btn-block-option" href="{{ route('admin.login') }}" data-toggle="tooltip" data-placement="left" title="Sign In">
                                <i class="fa fa-sign-in-alt"></i>
                            </a>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="p-sm-3 px-lg-4 py-lg-5">
                            <h1 class="mb-2">{{ config('app.name', 'Laravel') }} :: Admin</h1>
                            <p>Please fill the following details to reset password.</p>

                            <!-- Sign Up Form -->
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="js-validation-signup" method="POST" action="{{ route('admin.password.email') }}" aria-label="{{ __('Reset Password') }}">
                                @csrf
                                <div class="py-3">
                                    <div class="form-group">
                                        <input id="email" type="email" 
                                            class="form-control form-control-lg form-control-alt @error('email') is-invalid @enderror" 
                                            name="email" value="{{ old('email') }}" 
                                            placeholder="Email"
                                            required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-block btn-success">
                                            <i class="far fa-paper-plane mr-1"></i> {{ __('Send Password Reset Link') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Sign Up Form -->
                        </div>
                    </div>
                </div>
                <!-- END Sign Up Block -->
            </div>
        </div>
    </div>
    <div class="content content-full font-size-sm text-muted text-center">
        <strong>{{ config('app.name', 'Laravel') }}</strong> &copy; <span data-toggle="year-copy"></span>
    </div>
</div>
<!-- END Page Content -->
@endsection
