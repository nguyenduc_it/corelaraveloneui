@extends('admin.layouts.simple')

@section('content')
<!-- Page Content -->
<div class="hero-static">
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-4">
                <!-- Sign Up Block -->
                <div class="block block-themed block-fx-shadow mb-0">
                    <div class="block-header bg-success">
                        <h3 class="block-title">{{ __('Confirm Password') }}</h3>
                        <div class="block-options">
                            @if (Route::has('admin.password.request'))
                                    <a class="btn-block-option font-size-sm" href="{{ route('admin.password.request') }}">{{ __('Forgot Password?') }}</a>
                            @endif
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="p-sm-3 px-lg-4 py-lg-5">
                            <h1 class="mb-2">{{ config('app.name', 'Laravel') }} :: Admin</h1>
                            <p>{{ __('Please confirm your password before continuing.') }}</p>

                            <!-- Sign Up Form -->
                            <form class="js-validation-signup" method="POST" action="{{ route('admin.password.confirm') }}">
                                @csrf
                                <div class="py-3">
                                    <div class="form-group">
                                        <input id="password" type="password" 
                                            class="form-control form-control-lg form-control-alt @error('password') is-invalid @enderror" 
                                            name="password" required autocomplete="current-password" placeholder="Password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-block btn-success">
                                            <i class="far fa-paper-plane mr-1"></i> {{ __('Confirm Password') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Sign Up Form -->
                        </div>
                    </div>
                </div>
                <!-- END Sign Up Block -->
            </div>
        </div>
    </div>
    <div class="content content-full font-size-sm text-muted text-center">
        <strong>{{ config('app.name', 'Laravel') }}</strong> &copy; <span data-toggle="year-copy"></span>
    </div>
</div>
<!-- END Page Content -->
@endsection
