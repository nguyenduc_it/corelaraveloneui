@extends('admin.layouts.simple')

@section('content')
 <!-- Page Content -->
 <div class="bg-image" style="background-image: url('/media/photos/photo6@2x.jpg');">
    <div class="hero-static bg-white-95">
        <div class="content">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <!-- Sign In Block -->
                    <div class="block block-themed block-fx-shadow mb-0">
                        <div class="block-header">
                            <h3 class="block-title">Sign In</h3>
                            <div class="block-options">
                                @if (Route::has('admin.password.request'))
                                    <a class="btn-block-option font-size-sm" href="{{ route('admin.password.request') }}">{{ __('Forgot Password?') }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="p-sm-3 px-lg-4 py-lg-5">
                                <h1 class="mb-2">{{ config('app.name', 'Laravel') }} :: Admin</h1>
                                <p>Welcome, please login.</p>

                                <!-- Sign In Form -->
                                <form class="js-validation-signin" method="POST" action="{{ route('admin.login') }}" aria-label="{{ __('Login') }}">
                                    @csrf
                                    <div class="py-3">
                                        <div class="form-group">
                                            <input id="email" type="email" class="form-control form-control-alt form-control-lg @error('email') is-invalid @enderror" 
                                            name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                                            
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input id="password" type="password" class="form-control form-control-alt form-control-lg @error('password') is-invalid @enderror" 
                                                name="password" required autocomplete="current-password" placeholder="Password">
                                            
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label font-w400" for="remember">Remember Me</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-xl-5">
                                            <button type="submit" class="btn btn-block btn-primary">
                                                <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                    <!-- END Sign In Block -->
                </div>
            </div>
        </div>
        <div class="content content-full font-size-sm text-muted text-center">
            <strong>{{ config('app.name', 'Laravel') }}</strong> &copy; <span data-toggle="year-copy"></span>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
