@extends('admin.layouts.app')

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Admin :: Dashboard <small class="font-size-base font-w400 text-muted">You are logged in!</small>
            </h1>
        </div>
    </div>
</div>
@endsection
