## Daniel - duc@forsharing.info
## ToDu Forsharing Company Limited - hello@forsharing.info

## Base Project Laravel

Bộ code sử dụng Laravel 7 - OneUi -Bootstrap 4 - Jquery:

Bộ code dừng lại ở base dự án:
- Multi guard / Auth
- Roles/Permissions
- Api-JWT
- Manager Account Admin

## Route Login

- /admin
- /login
- /api
    - api/v1/login
    - api/v1/refresh
    - api/v1/logout
    - api/v1/me

## Seed create data
- php artisan db:seed --class=PermissionsSeed

## Liên hệ

Mọi thắc mắc liện hệ: duc@forsharing.info

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
