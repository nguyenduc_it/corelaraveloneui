<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminPermissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'account-list',
        ];

        $adminRoles = [
            'admin'
        ];


        foreach ($adminPermissions as $item) {
            \Spatie\Permission\Models\Permission::updateOrCreate(['name' => $item, 'guard_name' => 'admin']);
        }


        foreach ($adminRoles as $item) {
            \Spatie\Permission\Models\Role::updateOrCreate(['name' => $item, 'guard_name' => 'admin']);
        }

        $superadmins = \App\Models\Admin::create([
            'name' => 'super admin',
            'email' => 'super@admin.com',
            'is_super' => 1,
            'password' => bcrypt('123456')
        ]);

        $superadmins->assignRole('admin');

        foreach ($adminPermissions as $permission) {
            $p = \Spatie\Permission\Models\Permission::where('name', '=', $permission)->first();
            $role = \Spatie\Permission\Models\Role::where('name', '=', 'admin')->first();
            $p && $role->givePermissionTo($p);
        }
    }
}
