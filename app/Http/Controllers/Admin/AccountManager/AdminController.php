<?php
namespace App\Http\Controllers\Admin\AccountManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use DB;


class AdminController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware(['admin_permission:account-list', 'is_super'])->only(['index']);
        $this->middleware(['is_super'])->only([ 'create', 'store', 'edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(\Auth::guard('admin')->user()->can('account-list'));
        $users = Admin::get();
        $roles = Role::where('guard_name', 'admin')->get();
        $permissions = Permission::where('guard_name', 'admin')->get();

        return view('admin.accounts.admin_management.index', compact('users', 'roles', 'permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('guard_name', 'admin')->get();
        $permissions = Permission::where('guard_name', 'admin')->get();

        return view('admin.accounts.admin_management.create', compact('roles', 'permissions'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|max:120',
            'email' => 'required|email|unique:admins',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = Admin::create(array_merge($request->only('email', 'name'), ['password' => bcrypt($request->input('password'))]));

        if ($roles = $request->input('roles', [])) {
            foreach ($roles as $roleId) {
                $role = Role::where('id', '=', $roleId)->firstOrFail();
                $user->assignRole($role);
            }
        }

        if ($permissions = $request->input('permissions', [])) {
            foreach ($permissions as $permissionId) {
                $permission = Permission::where('id', '=', $permissionId)->firstOrFail();
                $user->givePermissionTo($permission);
            }
        }

        return redirect()->route('admin.admin_manager.index')
            ->with('success', 'Admin account has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::findOrFail($id);
        $roles = Role::where('guard_name', 'admin')->get();
        $permissions = Permission::where('guard_name', 'admin')->get();

        return view('admin.accounts.admin_management.edit', compact('roles', 'permissions', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::findOrFail($id);
        
        $request->validate([
            'name' => 'required|max:120',
            'email' => 'required|email|unique:admins,email,' . $id,
            'password' => 'nullable|min:6|confirmed'
        ]);

        $roles = $request->input('roles', []);
        $permissions = $request->input('permissions', []);

        $user->update($request->only(['name', 'email']));

        if ($request->get('password')) {
            $user->update(['password' => \Hash::make($request->get('password'))]);
        }

        if ($roles) {
            $user->roles()->sync($roles);
        }
        else {
            $user->roles()->detach();
        }

        if ($permissions) {
            $user->permissions()->sync($permissions);
        }
        else {
            $user->permissions()->detach();
        }

        return redirect()->route('admin.admin_manager.index')->with('success', 'Admin acount has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return \abort(404);
    }
}