<?php
namespace App\Http\Controllers\Admin\AccountManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;


class RoleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('admin_permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('admin_permission:role-create', ['only' => ['create','store']]);
        $this->middleware('admin_permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('admin_permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->paginate(5);
        return view('admin.accounts.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::orderBy('name')->get();
        $guards = config('custom.guards');
        return view('admin.accounts.roles.create',compact('permissions', 'guards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|unique:roles',
                'guard_name' => 'required',
            ]
        );

        $name = $request->input('name');
        $role = new Role();
        $role->name = $name;
        $role->guard_name = $request->input('guard_name');
        $permissions = $request->input('permissions', []);
        $role->save();
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->first();
            $role = Role::where('name', '=', $name)->first();
            $p && $role->givePermissionTo($p);
        }

        return redirect()->route('admin.roles.index')
                        ->with('success','Role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guards = config('custom.guards');
        $role = Role::findOrFail($id);
        $permissions = Permission::orderBy('name')->get();
        return view('admin.accounts.roles.edit', compact('role', 'permissions', 'guards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $id,
            'guard_name' => 'required',
        ]);

        $permissions = $request->input('permissions', []);
        $role->name = $request->input('name');
        $role->guard_name = $request->input('guard_name');
        $role->save();

        $p_all = Permission::all();
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }
        foreach ($permissions as $permission) {
            $p = Permission::where(['id' => $permission, 'guard_name' => $role->guard_name])->first();
            $p && $role->givePermissionTo($p);
        }

        return redirect()->route('admin.roles.index')
                        ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect()->route('admin.roles.index')
                        ->with('success','Role deleted successfully');
    }
}