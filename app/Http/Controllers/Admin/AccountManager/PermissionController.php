<?php
namespace App\Http\Controllers\Admin\AccountManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;


class PermissionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('admin_permission:role-list|role-create|role-edit|role-delete', ['only' => ['index']]);
         $this->middleware('admin_permission:role-create', ['only' => ['create','store']]);
         $this->middleware('admin_permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('admin_permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::orderBy('id','DESC')->paginate(5);
        return view('admin.accounts.permissions.index',compact('permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guards = config('custom.guards');
        $roles = Role::get();
        return view('admin.accounts.permissions.create', compact('roles', 'guards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:40',
        ]);


        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;
        $permission->guard_name = $request->input('guard_name');
        $roles = $request->input('roles', []);

        $permission->save();
        if ($roles) {
            foreach ($roles as $role) {
                $r = Role::where(['id' => $role, 'guard_name' => $permission->guard_name])->first();
                $permission = Permission::where('name', '=', $name)->first();
                $permission && $r && $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('admin.permissions.index')
                        ->with('success','Permission created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin.permissions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guards = config('custom.guards');
        $permission = Permission::find($id);

        return view('admin.accounts.permissions.edit', compact('permission', 'guards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:40',
        ]);

        $permission->name = $request->input('name');
        $permission->guard_name = $request->input('guard_name');
        $permission->save();

        return redirect()->route('admin.permissions.index')
                        ->with('success','Permission updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        if ($permission->name == "roles_and_permissions") {
            return redirect()->route('admin.permissions.index')
                ->with('success', 'Can not delete this permission.');
        }

        $permission->delete();

        return redirect()->route('admin.permissions.index')
                        ->with('success','Permission deleted successfully');
    }
}