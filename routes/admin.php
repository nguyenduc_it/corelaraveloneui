<?php

Route::name('admin.')->group(function () {
    Route::group(['namespace' => 'Admin'], function() {
        // Dashboard
        Route::get('/', 'HomeController@index')->name('home');
    
        // Login
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    
        // Register
        // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
        // Route::post('register', 'Auth\RegisterController@register');
    
        // Reset Password
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
    
        // Confirm Password
        Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
        Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');
    
        // Verify Email
        // Route::get('email/verify', 'Auth\VerificationController@show')->name('admin.verification.notice');
        // Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('admin.verification.verify');
        // Route::post('email/resend', 'Auth\VerificationController@resend')->name('admin.verification.resend');
    
    
        Route::group(['namespace' => 'AccountManager', 'middleware' => ['admin.auth']], function() {
            Route::resource('roles','RoleController');
            Route::resource('permissions','PermissionController');
            Route::resource('admin_manager','AdminController');
        });
    });
});

