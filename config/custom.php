<?php 

return [
    'guards' => [
        'admin' => 'Admin',
        'user' => 'User',
    ],
    
    'settings' => [
        'paginate' => 20
    ],
];