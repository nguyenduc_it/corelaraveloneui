/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/admin_dialogs.js":
/*!*********************************************!*\
  !*** ./resources/js/pages/admin_dialogs.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// SweetAlert2, for more examples you can check out https://github.com/sweetalert2/sweetalert2
var adminDialogs = /*#__PURE__*/function () {
  function adminDialogs() {
    _classCallCheck(this, adminDialogs);
  }

  _createClass(adminDialogs, null, [{
    key: "sweetAlert2",

    /*
     * SweetAlert2 demo functionality
     *
     */
    value: function sweetAlert2() {
      // Set default properties
      var toast = Swal.mixin({
        buttonsStyling: false,
        customClass: {
          confirmButton: 'btn btn-success m-1',
          cancelButton: 'btn btn-danger m-1',
          input: 'form-control'
        }
      }); // // Init a simple dialog on button click
      // jQuery('.js-swal-simple').on('click', e => {
      //     toast.fire('Hi, this is just a simple message!');
      // });
      // // Init an success dialog on button click
      // jQuery('.js-swal-success').on('click', e => {
      //     toast.fire('Success', 'Everything was updated perfectly!', 'success');
      // });
      // // Init an info dialog on button click
      // jQuery('.js-swal-info').on('click', e => {
      //     toast.fire('Info', 'Just an informational message!', 'info');
      // });
      // Init an warning dialog on button click

      jQuery('.btn-delete-permission').on('click', function (e) {
        toast.fire('Warning', 'Something needs your attention!', 'warning');
      }); // // Init an error dialog on button click
      // jQuery('.js-swal-error').on('click', e => {
      //     toast.fire('Oops...', 'Something went wrong!', 'error');
      // });
      // // Init an question dialog on button click
      // jQuery('.js-swal-question').on('click', e => {
      //     toast.fire('Question', 'Are you sure about that?', 'question');
      // });
      // // Init an example confirm dialog on button click
      // jQuery('.js-swal-confirm').on('click', e => {
      //     toast.fire({
      //         title: 'Are you sure?',
      //         text: 'You will not be able to recover this imaginary file!',
      //         icon: 'warning',
      //         showCancelButton: true,
      //         customClass: {
      //             confirmButton: 'btn btn-danger m-1',
      //             cancelButton: 'btn btn-secondary m-1'
      //         },
      //         confirmButtonText: 'Yes, delete it!',
      //         html: false,
      //         preConfirm: e => {
      //             return new Promise(resolve => {
      //                 setTimeout(() => {
      //                     resolve();
      //                 }, 50);
      //             });
      //         }
      //     }).then(result => {
      //         if (result.value) {
      //             toast.fire('Deleted!', 'Your imaginary file has been deleted.', 'success');
      //             // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      //         } else if (result.dismiss === 'cancel') {
      //             toast.fire('Cancelled', 'Your imaginary file is safe :)', 'error');
      //         }
      //     });
      // });
      // // Init an example confirm alert on button click
      // jQuery('.js-swal-custom-position').on('click', e => {
      //     toast.fire({
      //         position: 'top-end',
      //         title: 'Perfect!',
      //         text: 'Nice Position!',
      //         icon: 'success'
      //     });
      // });
    }
    /*
     * Init functionality
     *
     */

  }, {
    key: "init",
    value: function init() {
      this.sweetAlert2();
    }
  }]);

  return adminDialogs;
}(); // Initialize when page loads


jQuery(function () {
  adminDialogs.init();
});

/***/ }),

/***/ 3:
/*!***************************************************!*\
  !*** multi ./resources/js/pages/admin_dialogs.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/vagrant/code/PHP/Elearning/resources/js/pages/admin_dialogs.js */"./resources/js/pages/admin_dialogs.js");


/***/ })

/******/ });